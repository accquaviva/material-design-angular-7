import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.css']
})
export class PropertyBindingComponent implements OnInit {

  color = 'accent';
  btnDisabled = true;
  colors = ['primary', 'accent', 'warn', ''];
  idx = 0;

  constructor() { }

  ngOnInit() {
    // funcao que fica alterando as cores do button
    setInterval( () => {
      this.idx = (this.idx + 1) % this.color.length;
    }, 500);

  }

}

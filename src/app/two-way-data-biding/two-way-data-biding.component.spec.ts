import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoWayDataBidingComponent } from './two-way-data-biding.component';

describe('TwoWayDataBidingComponent', () => {
  let component: TwoWayDataBidingComponent;
  let fixture: ComponentFixture<TwoWayDataBidingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoWayDataBidingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoWayDataBidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
